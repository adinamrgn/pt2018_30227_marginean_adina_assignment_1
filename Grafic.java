package Grafica;

import Calculator.*;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

class Grafic extends JPanel {
    public static void main(String args[]) {

        /* Am 3 textfield-uri, 2 pt polinoamele destinate adunarii, inmultirii,
        scaderii, si un textfield pt polinomul destinat derviarii si integrarii, ca sa nu apara
        confuzii la preluarea datelor pt efectuarea operatiilor;
        Am impartit toate elementele in panel-uri in asa fel incat fereastra principala sa arate cat mai
        prietenoasa si usor de folosit.
         */
        JFrame frame = new JFrame ("Calculul Polinoamelor");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(700, 350);

        JPanel panel1 = new JPanel();
        JPanel panel2 = new JPanel();
        JPanel panel3 = new JPanel();
        JPanel panel4 = new JPanel();
        JPanel panel5 = new JPanel();
        JPanel principal = new JPanel();

        JLabel l1 = new JLabel ("   Introduceti polinom 1: ");
        JLabel l2 = new JLabel ("   Introduceti polinom 2: ");
        JLabel l3 = new JLabel ("   Introduceti polinomul de derivat/integrat: " );
        JLabel rezLabel = new JLabel ("REZULTAT");

        JTextField pol1Text = new JTextField(" ");
        JTextField pol2Text = new JTextField(" ");
        JTextField pol3Text = new JTextField();

        panel1.add(l1);
        panel1.add(l2);
        panel1.add(pol1Text);
        panel1.add(pol2Text);
        panel1.setLayout(new GridLayout(2,2));

        JButton butonAdunare = new JButton("Adunare");
        JButton butonScadere = new JButton("Scadere");
        JButton butonInmultire = new JButton("Inmultire");
        JButton butonImpartire = new JButton("Impartire");
        JButton butonDerivare = new JButton("Derivare");
        JButton butonIntegrare = new JButton("Integrare");

        panel2.add(butonAdunare);
        panel2.add(butonScadere);
        panel2.add(butonInmultire);
        panel2.add(butonImpartire);
        panel2.setLayout(new GridLayout(2,2));

        panel3.add(l3);
        l3.setHorizontalAlignment((int)CENTER_ALIGNMENT);
        panel3.add(pol3Text);
        panel3.setLayout(new GridLayout(2,1));

        panel4.add(butonDerivare);
        panel4.add(butonIntegrare);
        panel4.setLayout(new GridLayout(1,2));

        panel5.add(rezLabel);
        panel5.setLayout(new FlowLayout());

        principal.add(panel1);
        principal.add(panel2);
        principal.add(panel3);
        principal.add(panel4);
        principal.add(panel5);
        principal.setLayout(new BoxLayout(principal, BoxLayout.Y_AXIS));

        frame.setContentPane(principal);
        frame.setVisible(true);


        Operatii operatii = new Operatii();


        butonAdunare.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Prelucrare prelucrare = new Prelucrare();
                Polinom polinom1 = prelucrare.preluareDate(pol1Text.getText());
                Polinom polinom2 = prelucrare.preluareDate(pol2Text.getText());
                Polinom polinomRez;
                polinomRez = operatii.adunare(polinom1, polinom2);
                rezLabel.setText(prelucrare.afisare(polinomRez));
            }
        });

        butonScadere.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Prelucrare prelucrare = new Prelucrare();
                Polinom polinom1 = prelucrare.preluareDate(pol1Text.getText());
                Polinom polinom2 = prelucrare.preluareDate(pol2Text.getText());
                Polinom polinomRez;
                polinomRez = operatii.scadere(polinom1, polinom2);
                rezLabel.setText(prelucrare.afisare(polinomRez));
            }
        });

        butonInmultire.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Prelucrare prelucrare = new Prelucrare();
                Polinom polinom1 = prelucrare.preluareDate(pol1Text.getText());
                Polinom polinom2 = prelucrare.preluareDate(pol2Text.getText());
                Polinom polinomRez;
                polinomRez = operatii.inmultire(polinom1, polinom2);
                rezLabel.setText(prelucrare.afisare(polinomRez));
            }
        });

        butonDerivare.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Prelucrare prelucrare = new Prelucrare();
                Polinom polinom1 = prelucrare.preluareDate(pol3Text.getText());
                Polinom polinomRez;
                polinomRez = operatii.derivare(polinom1);
                rezLabel.setText(prelucrare.afisare(polinomRez));
            }
        });

        butonIntegrare.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Prelucrare prelucrare = new Prelucrare();
                Polinom polinom1 = prelucrare.preluareDate(pol3Text.getText());
                String polinomRez;
                polinomRez = operatii.integrare(polinom1);
                rezLabel.setText(polinomRez);
            }
        });
    }


}
