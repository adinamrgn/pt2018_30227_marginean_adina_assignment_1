package Calculator;

import java.lang.*;

public class Monom implements Comparable<Monom>{
    private int coeficient;
    private int putere;

    public Monom(int coeficient, int putere){
        this.coeficient = coeficient;
        this.putere = putere;
    }

    public int getCoeficient() {
        return coeficient;
    }

    public int getPutere() {
        return putere;
    }

    public void setCoeficient(int coeficient) {
        this.coeficient = coeficient;
    }

    @Override // implementez metoda apartinand interfetei Comparable, ca sa imi pot ordona monoamele descrescator, dupa putere
    public int compareTo(Monom o) {
        if (putere > o.getPutere()) {
            return -1;
        }
        if (putere == o.getPutere()) {
            return 0;
        }
        return 1;
    }



}
