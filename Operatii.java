package Calculator;
import java.util.*;

public class Operatii {


    public Polinom adunare(Polinom p1, Polinom p2){
        Polinom suma = new Polinom();
        List<Monom> monoame1 = p1.getMonoame();
        List<Monom> monoame2 = p2.getMonoame();
        int verificare = 0; //0 = nu am gasit nimic, 1 = am gasit puterea echivalenta in pol2 si am facut calculele
        for (Monom monom: monoame1){
            verificare = 0;
            for(int i = 0; i < monoame2.size(); i++){  //nu merge cu foreach pt ca se lucreaza la dimensiunea listei in bucla
                if (monom.getPutere() == monoame2.get(i).getPutere()){
                    int sumaCoef = monom.getCoeficient() + monoame2.get(i).getCoeficient(); // daca gasesc monoame cu acelasi grad in pol 2, le insumez coef cu cel al monoamelor din pol 1
                    Monom rezultat = new Monom(sumaCoef, monom.getPutere());
                    suma.adaugaMonom(rezultat);
                    monoame2.remove(i); //sterg monoamele din al doilea polinom, pentru care am facut deja adunarea cu coef monoamelor din pol 1
                    verificare = 1;
                }
            }
            if (verificare == 0){ //daca nu am gasit nicio echivalenta de grad in pol 2, atunci iau pur si simplu monomul respectiv si il adaug la polinomul suma asa cum e
                suma.adaugaMonom(monom);
            }
        }
        for (Monom monom: monoame2){ //fac acelasi lucru si pt monoamele ramase in pol 2, care sigur nu au echivalent de grad in pol 1
            suma.adaugaMonom(monom);
        }
        return suma;
    }

    public Polinom scadere(Polinom p1, Polinom p2){ //functioneaza pe acelasi principiu cu adunarea
        Polinom diferenta = new Polinom();
        List<Monom> monoame1 = p1.getMonoame();
        List<Monom> monoame2 = p2.getMonoame();
        int verificare = 0;
        for (Monom monom: monoame1){
            verificare = 0;
            for (int i = 0; i < monoame2.size(); i++) {
                if (monom.getPutere() == monoame2.get(i).getPutere()) {
                    int difCoef = monom.getCoeficient() - monoame2.get(i).getCoeficient();
                    Monom rezultat = new Monom(difCoef, monom.getPutere());
                    diferenta.adaugaMonom(rezultat);
                    monoame2.remove(i);
                    verificare = 1;
                }
            }
            if (verificare == 0) {
                diferenta.adaugaMonom(monom);
            }
        }
        for (Monom monom: monoame2){
            monom.setCoeficient(monom.getCoeficient() * (-1)); /*aici, monoamele din pol 2 sunt adaugate cu semn schimbat
            la polinomul rezultat, pentru ca scaderea se face ca pol1-pol2*/
            diferenta.adaugaMonom(monom);
        }
        return diferenta;
    }

    public Polinom inmultire(Polinom p1, Polinom p2){
        Polinom produs = new Polinom();
        List<Monom> monoame1 = p1.getMonoame();
        List<Monom> monoame2 = p2.getMonoame();
        for (Monom monom1: monoame1){
            for (Monom monom2: monoame2){
                Monom rezultat = new Monom(monom1.getCoeficient() * monom2.getCoeficient(), monom1.getPutere() + monom2.getPutere());
                produs.adaugaMonom(rezultat);
            }

        }
        return produs;
    }

    public Polinom derivare(Polinom p1){
        Polinom derivat = new Polinom();
        List<Monom> monoame = p1.getMonoame();

        for (Monom monom: monoame){
            if (monom.getPutere() == 0){
                continue;
            }
            int coef = monom.getPutere() * monom.getCoeficient(); //formula derivarii la monoame, coeficientul se inmulteste cu puterea
            int ptr = monom.getPutere() - 1; //puterea scade cu 1
            Monom rezultat = new Monom(coef, ptr);
            derivat.adaugaMonom(rezultat);
        }
        return derivat;
    }

    public String integrare(Polinom p1){
        String integrat = "";
        p1.sortare();
        List<Monom> monoame = p1.getMonoame();
        // construiesc un String ca sa pot afisa coeficientul fiecarui monom rezultat, sub forma de fractie
        for (Monom monom: monoame){
            if (monom.getCoeficient() > 0) {  //daca am coef pozitiv, semnul "+" nu este implicit, deci trebuie sa il pun eu
                integrat += "+";
            }
            integrat += monom.getCoeficient(); //concatenez valoarea coeficientului
            if (monom.getPutere() > 0){ /*daca am variabila, atunci o sa am si numitor la rezultat; daca nu am, dupa integrare o
            sa am x^1, si nu mai are sens sa pun coeficientul initial supra 1*/
                integrat += "/" + (monom.getPutere() + 1);
            }
            integrat += "x^" + (monom.getPutere() + 1); //concatenez la sir si valoarea puterii+1
        }
        return integrat;
    }

}
