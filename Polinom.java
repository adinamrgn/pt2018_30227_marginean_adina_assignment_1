package Calculator;
import java.util.*;

public class Polinom {

    private List<Monom> monoame = new ArrayList<Monom>(); //polinomul se retine sub forma unei liste de monoame

    public void adaugaMonom(Monom noulMonom){
        if (noulMonom.getCoeficient() == 0) return; //daca monomul pe care incerc sa il adaug are coeficientul 0, trec peste

        for (int i = 0; i < monoame.size(); i++){
            Monom monom = monoame.get(i);
            if (monom.getPutere() == noulMonom.getPutere()){  //daca mai exista monom cu acelasi grad, doar actualizez coeficientul monomului deja existent
                int coeficient = (noulMonom.getCoeficient() + monom.getCoeficient());
                if (coeficient == 0) {
                    monoame.remove(monom); //daca dupa ce am actualizat coeficientul, acesta este 0, elimin monomul respectiv
                    return;
                }
                else {
                    monom.setCoeficient(coeficient);
                    return;
                }
            }
        }
        monoame.add(noulMonom);

    }

    public List<Monom> getMonoame() {
        return monoame;
    }

    public void sortare(){
        Collections.sort(monoame);
    }
}


