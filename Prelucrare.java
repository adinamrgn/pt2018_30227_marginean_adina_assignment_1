package Grafica;

import Calculator.*;
import java.util.*;
import java.util.regex.*;

public class Prelucrare {

    public Polinom preluareDate(String polinomText) {

        polinomText = polinomText.trim(); //elimin spatiile din input, de la inceput si final, daca sunt
        Pattern pattern = Pattern.compile("([-\\+])([0-9]*)x(\\^([1-9][0-9]*))?|([-\\+])([0-9]+)");
        Matcher mtch = pattern.matcher(polinomText);
        Polinom polinom = new Polinom();

        while (mtch.find()) {
            int coeficient = 0;
            int putere = 0;

            //gr1 = semn; gr2 = coef; gr4=puterea; gr5=semnul; gr6=coef;
            if (mtch.group(2) == null && mtch.group(4) == null) { //daca am cazul in care monomul este un termen liber
                if (mtch.group(5).equals("-")) {  //verific semnul termenului, si actualizez coef dupa caz
                    coeficient = (Integer.parseInt(mtch.group(6)) * (-1));
                } else {
                    coeficient = (Integer.parseInt(mtch.group(6)));
                }
                putere = 0; //in cazul acesta, puterea nu exista (pentru ca nu avem deloc variabila x)
            } else {
                if (mtch.group(6) == null) {  //cazul in care exista si variabila
                    if (mtch.group(1).equals("-")) {
                        coeficient = (Integer.parseInt(mtch.group(2)) * (-1));

                    } else {
                        coeficient = (Integer.parseInt(mtch.group(2)));

                    }
                    if (mtch.group(4) == null) { //verific daca am termen pe pozitia puterii
                        putere = 1; //daca nu am, atunci puterea o sa fie implicit 1
                    } else {
                        putere = (Integer.parseInt(mtch.group(4)));  //altfel, iau termenul de pe pozitia respectiva si il salvez ca putere
                    }
                }
            }
            Monom monom = new Monom(coeficient, putere);
            polinom.adaugaMonom(monom);
        }
        return polinom;
    }

    public String afisare(Polinom polinom) {
        List<Monom> monoame = polinom.getMonoame();
        String polText = "";
        polinom.sortare();
        /* o sa merg pe cazuri si o sa verific mai intai daca valoarea coef este pozitiva, sau negativa. daca este pozitiva,
        trebuie sa adaug mai intai semnul "+", si apoi sa fac verificarile pe putere (daca este 0, nu mai trec variabila x,
        daca este 1, nu mai trec si caracterul "^", iar daca e mai mare ca 1, trec atat "x" cat si "^"; la fel si in cazul in
        care coef este negativ, doar ca aici semnul o sa fie concatenat implicit cu coeficientul
         */
        for (Monom monom : monoame) {
            if (monom.getCoeficient() < 0) {
                if (monom.getPutere() == 0) {
                    polText += monom.getCoeficient();
                } else {
                    if (monom.getPutere() == 1) {
                        polText += monom.getCoeficient() + "x";
                    } else {
                        polText += monom.getCoeficient() + "x^" + monom.getPutere();
                    }
                }
            } else {
                if (monom.getPutere() == 0) {
                    polText += "+" + monom.getCoeficient();
                } else {
                    if (monom.getPutere() == 1) {
                        polText += "+" + monom.getCoeficient() + "x";
                    } else {
                        polText += "+" + monom.getCoeficient() + "x^" + monom.getPutere();
                    }
                }
            }

        }
        return polText;
    }
}

